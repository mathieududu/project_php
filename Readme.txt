how to run the app :

1) Set the database :

- I used MAMP (or WAMP) and phpmyadmin (mySql) in local
- In laravel_app/.env write the correct information in DB area :

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=? 
DB_DATABASE=?
DB_USERNAME=?
DB_PASSWORD=?


- In the terminal :

	1. php artisan migrate

=> you should have the table "list" with three colomn "Name", "Email", "School" (and "id" as well).

2) Go on:
		
		localhost/laravel_app/public/user

		(if it doesn't work you can try localhost/laravel_app/public/index.php/user)



