<?php

namespace App\Repositories;

use App\Users;

class UserRepository
{

    protected $user;

    public function __construct(Users $user)
	{
		$this->user = $user;
	}

	private function save(Users $user, Array $inputs) //function edit
	{
		$user->name = $inputs['name'];
		$user->email = $inputs['email'];	
		$user->school = $inputs['school'];

	
		$user->school = implode(',', $user->school);

		$user->save();
	}

	public function getPaginate($n) //function index
	{
		return $this->user->paginate($n);
	}

	public function store(Array $inputs) //function create
	{
		$user = new $this->user;
		
		$user->name = $inputs['name'];
		$user->email = $inputs['email'];	
		$user->school = $inputs['school'];
		$user->school = implode(',', $user->school);

		

		$this->save($user, $inputs);

		return $user;
	}

	public function getById($id)
	{
		return $this->user->findOrFail($id);
	}

	public function update($id, Array $inputs) //function edit
	{
		$this->save($this->getById($id), $inputs);
	}

	public function destroy($id)
	{
		$this->getById($id)->delete();
	}

}